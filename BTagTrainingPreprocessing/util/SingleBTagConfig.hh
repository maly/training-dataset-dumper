#ifndef SINGLEBTAG_CONFIG_HH
#define SINGLEBTAG_CONFIG_HH

#include "DL2Config.hh"

#include <string>
#include <vector>
#include <map>
#include "TrackSelectorConfig.hh"

enum class TrackSortOrder {ABS_D0_SIGNIFICANCE, ABS_D0, D0_SIGNIFICANCE};

typedef std::map<std::string,std::vector<std::string>> VariableList;

struct SingleBTagConfig {
  std::string jet_collection;
  std::string jet_calibration_collection;
  std::string jet_calib_file;
  std::string cal_seq;
  std::string cal_area;
  bool do_calibration;
  bool run_augmenters;
  bool vr_cuts;
  float jvt_cut;
  float pt_cut;
  std::vector<DL2Config> dl2_configs;
  VariableList btag;
  VariableList track;
  size_t n_tracks_to_save;
  TrackSortOrder track_sort_order;
  TrackSelectorConfig track_select_cfg;
};

SingleBTagConfig get_singlebtag_config(const std::string& config_file_name);


#endif
